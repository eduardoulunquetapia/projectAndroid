package clase.velo.cice.libreriaquickscroll2;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.ViewGroup;

import clase.velo.cice.libreriaquickscroll2.adapter.QuickScrollPagerAdapter;
import clase.velo.cice.libreriaquickscroll2.fragment.ContactListFragment;
import clase.velo.cice.libreriaquickscroll2.fragment.CountryFragment;
import clase.velo.cice.libreriaquickscroll2.fragment.EventListFragment;
import clase.velo.cice.libreriaquickscroll2.fragment.InfoFragment;
import clase.velo.cice.libreriaquickscroll2.fragment.MovieListFragment;

public class MainActivity extends FragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final ViewGroup layout = (ViewGroup) getLayoutInflater().inflate(R.layout.activity_main, null);

		final ViewPager pager = (ViewPager) layout.findViewById(R.id.main_pager);

		final QuickScrollPagerAdapter adapter = new QuickScrollPagerAdapter(getSupportFragmentManager(), this);
		adapter.addPage("base_popup_holo_nohandle", ContactListFragment.class, null);
		adapter.addPage("expandable_indicator_holo_nohandle", CountryFragment.class, null);
		adapter.addPage("base_indicator_holo_handle", EventListFragment.class, null);
		adapter.addPage("base_popup_customlayout_colored_nohandle", MovieListFragment.class, null);
		adapter.addPage("info", InfoFragment.class, null);

		pager.setAdapter(adapter);

		setContentView(layout);
	}

}
