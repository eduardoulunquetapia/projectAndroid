package com.duard.dynamiclistjavathreading;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.IntegerRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private TextView numberListTextView;
    private String TAG = "MainActivity";
    private NumberListHandler mHandler;
    private ProgressBar pBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        numberListTextView = (TextView) findViewById(R.id.nunericListTextView);
        mHandler = new NumberListHandler();
        //barra de pprogreso
        pBar = (ProgressBar) findViewById(R.id.progressbar);
        pBar.setVisibility(ProgressBar.INVISIBLE);
        pBar.setProgress(0);

    }

    public void startRandomList(View v) {

        new Thread(new NumberListRunnable()).start();
    }

    public class NumberListRunnable implements Runnable {


        //codificaria  la rutina del hilo
        @Override
        public void run() {
            Message m;
            //instanciamos un Random
            Random rnd = new Random();
            //hilo aparte para la barra de progreso
            m = mHandler.obtainMessage(mHandler.SHOW_PROGRESS);
            mHandler.sendMessage(m);

            for (int v=0;v<pBar.getMax();v++){
                int value = rnd.nextInt(1000000);
                Log.d(TAG, "value:  " + value);
                m = mHandler.obtainMessage(mHandler.SET_PROGRESS, new Integer(value));
                mHandler.sendMessage(m);
                // numberListTextView.append("\n" + value);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
            m=mHandler.obtainMessage(mHandler.HIDE_PROGRESS);
            mHandler.sendMessage(m);
        }
    }

    public class NumberListHandler extends Handler {

        public static final int SHOW_PROGRESS = 2;
        public static final int HIDE_PROGRESS = 3;
        public static final int SET_PROGRESS = 4;

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case SET_PROGRESS:
                    Integer value = (Integer) msg.obj;
                    numberListTextView.append("\n" + value);
                    pBar.setProgress(pBar.getProgress()+1);
                    break;
                case SHOW_PROGRESS:
                    pBar.setVisibility(View.VISIBLE);
                    break;
                case HIDE_PROGRESS:
                    pBar.setVisibility(View.GONE);
                    break;
            }
        }
    }
}
