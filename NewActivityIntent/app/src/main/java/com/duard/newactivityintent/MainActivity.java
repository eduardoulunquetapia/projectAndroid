package com.duard.newactivityintent;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void starNewActivity(View vista){
        Intent intent = new Intent(this,Activity2.class);
        intent.putExtra(Activity2.EXTRA_KEY,"Eduardo Ulunque Tapia");
        startActivity(intent);
    }
    public  void launchImplicitoHTTPIntent(View vista){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("http://www.mit.edu"));
        startActivity(intent);
    }
    public  void launchImplicitoGeoIntent(View vista){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("geo:0,0?q=Calle+de+Guzm%C3%A1n+el+Bueno,+54,+28015+Madrid/40.4467046,-3.7052572"));
             /*   "geo:40.43628717336835,-3.7130382657051086"+"?q="+"Calle+de+Guzm%C3%A1n+el+Bueno,+54,+28015+Madrid"));*/
                /*"geo:0,00,0?"+
        "q=1600+Pennsilvania+Ave+Washington+D"));*/
        startActivity(intent);
    }
    //http://www.elmayorportaldegerencia.com/Documentos/Biografias/[PD]%20Documentos%20-%20Julio%20Cesar.pdf
    public  void launchImplicitoHTTPMimeIntent(View vista){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("https://www.sixt.es/fileadmin/user_upload/alquiler-de-coches-de-lujo-sixt.jpg"));
        //intent.setType("image/*");
        if(intent.resolveActivity(getPackageManager())!=null){
            startActivity(intent);
        }

    }
    public void newActivityAndFloating (View v){
        Intent intent =new Intent(this,Activity3.class);
        intent.putExtra(Activity3.EXTRA_KEY, getResources().getString(R.string.link_mit));
        startActivity(intent);
    }
}
