package com.duard.newactivityintent;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.IntDef;
import android.support.annotation.StringDef;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Activity3 extends AppCompatActivity {
    public static final String EXTRA_KEY = "string";
    private static Intent intent;
    private static TextView boxTextView;
    private static FloatingActionButton btnBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);

        intent = getIntent();
        boxTextView = (TextView) findViewById(R.id.idTextView);
        btnBtn = (FloatingActionButton) findViewById(R.id.idFloatButton);
        boxTextView.setEnabled(false);

        btnBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boxTextView.setText(intent.getStringExtra(EXTRA_KEY));
                boxTextView.setEnabled(true);
                boxTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(getResources().getString(R.string.link_mit)));
                        startActivity(intent);
                    }
                });

            }
        });
        boxTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(getResources().getString(R.string.link_mit)));
                startActivity(intent);
            }
        });
    }
}
