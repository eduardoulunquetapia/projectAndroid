package com.duard.androidcontentprovider;

/**
 * Created by cice on 9/2/17.
 */

public class Flavor {
    String name;
    String description;
    //cambiamos int a String(sera url)
    String image;

    public Flavor(String name, String description, String image){
        this.name = name;
        this.image = image;
        this.description = description;
    }
}
