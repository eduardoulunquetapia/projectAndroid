package com.duard.stringlistview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {



    private String[] names={"edu","helmi","tere","jose","luis","negre"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ListView nameLayoutView=(ListView) findViewById(R.id.nameListView);
        nameLayoutView.setAdapter(new NameAdapter(this, R.layout.row_layout, names));


    }
    public class NameAdapter extends ArrayAdapter<String>{


        private Context context;
        public NameAdapter(Context context, int resource, String[] objects) {
            super(context, resource, objects);
            this.context=context;
        }

        @Override
        public int getCount() {
            return names.length;
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater=
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row=inflater.inflate(R.layout.row_layout, null);
            TextView nameTextView =(TextView)row.findViewById(R.id.nameTextView);
            nameTextView.setText(names[position]);

            return row;
        }


    }
}
