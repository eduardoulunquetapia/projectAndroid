package com.duard.appcalculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView display;
    private Integer res = null;
    private int operando;
    private String operador;
    private int state;
    public final static int INTRO_VALUES = 0;
    public final static int INTRO_OPERATIONS = 0;
    private int cadenaNum1;
    private int cadenaNum2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        display = (TextView) findViewById(R.id.display);

     /*   Button[] operationButton = {(Button) findViewById(R.id.btnMinus),
                (Button) findViewById(R.id.btnPlus),
                (Button) findViewById(R.id.btnPor),
                (Button) findViewById(R.id.dtnDiv),
                (Button) findViewById(R.id.btnPlusMinus),
                (Button) findViewById(R.id.btnComa),
                (Button) findViewById(R.id.btnEqual),
                (Button) findViewById(R.id.btnAC),
                (Button) findViewById(R.id.btnPorcentaje)
        };*/

        Button[] numberButton = {(Button) findViewById(R.id.btn0)
                , (Button) findViewById(R.id.btn1)
                , (Button) findViewById(R.id.btn2)
                , (Button) findViewById(R.id.btn3)
                , (Button) findViewById(R.id.btn4)
                , (Button) findViewById(R.id.btn5)
                , (Button) findViewById(R.id.btn6)
                , (Button) findViewById(R.id.btn7)
                , (Button) findViewById(R.id.btn8)
                , (Button) findViewById(R.id.btn9)
                , (Button) findViewById(R.id.btn0),
                (Button) findViewById(R.id.btnMinus),
                (Button) findViewById(R.id.btnPlus),
                (Button) findViewById(R.id.btnPor),
                (Button) findViewById(R.id.dtnDiv),
                (Button) findViewById(R.id.btnPlusMinus),
                (Button) findViewById(R.id.btnComa),
                (Button) findViewById(R.id.btnEqual),
                (Button) findViewById(R.id.btnAC),
                (Button) findViewById(R.id.btnPorcentaje)

        };


        for (final Button btn : numberButton) {
            btn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    switch (btn.getText().toString()) {
                        case "+":
                        case "-":
                        case "/":
                        case "X":
                            operar(btn);
                            break;
                        case "AC":
                            borrar(btn);
                            break;
                        case "+/-":
                        case "%":
                        case "=":
                            resultado(btn);
                            break;
                        default:
                            sumandoString(btn);
                    }

                }
            });
        }
    }

    private void resultado(Button btn) {
        cadenaNum2 = (Integer.parseInt(display.getText().toString()));
        switch (operador) {
            case "+":
                res = cadenaNum1 + cadenaNum2;
                display.setText(res.toString());
                break;
            case "-":
                res = cadenaNum1 - cadenaNum2;
                display.setText(res.toString());
                break;
            case "/":
                res = cadenaNum1 / cadenaNum2;
                display.setText(res.toString());
                break;
            case "X":
                res = cadenaNum1 * cadenaNum2;
                display.setText(res.toString());
                break;
        }
    }

    private void operar(Button btn) {
        cadenaNum1 = (Integer.parseInt(display.getText().toString()));
        display.setText("");
        operador = btn.getText().toString();
    }

    private void borrar(Button btn) {
        display.setText("");
    }

    private void sumandoString(Button btn) {
        if ((display.getText() != null) || display.getText() != "0") {
            if ((String) btn.getText() == "0") {
                display.setText(btn.getText().toString().replaceAll("^0+", ""));
            } else {
                display.setText(display.getText().toString() + btn.getText().toString());
            }
        } else {
            display.setText(btn.getText().toString());
        }
    }
}
