package com.duard.camaraconsumer;


import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.util.Set;

import static android.provider.MediaStore.ACTION_IMAGE_CAPTURE;

public class MainActivity extends AppCompatActivity {
    public static final int CAMARA_REQUEST = 1;
    private static final String TAG = MainActivity.class.getCanonicalName();
    private ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        image = (ImageView) findViewById(R.id.idImageView);
    }

    public void capturePhoto(View view) {
        Intent intent = new Intent(ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMARA_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "intent query responded...");
        switch (requestCode) {
            case CAMARA_REQUEST:
                Log.d(TAG, "camara,,,");
                if (requestCode == RESULT_OK) {
                    Log.d(TAG, " data.getData() :" + data.getData());
                    Set<String> keySet = data.getExtras().keySet();
                    for (String key : keySet) {
                        Log.d(TAG, " " + key + " : " + data.getExtras().getString(key));
                    }
                    Bitmap bmp = (Bitmap) data.getExtras().get("data");
                    image.setImageBitmap(bmp);
                } else {
                    Log.d(TAG, "error");
                }
                break;
        }
    }
}
