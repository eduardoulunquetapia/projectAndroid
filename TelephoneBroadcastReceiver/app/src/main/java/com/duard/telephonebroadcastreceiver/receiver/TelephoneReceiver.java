package com.duard.telephonebroadcastreceiver.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

public class TelephoneReceiver extends BroadcastReceiver {
    public TelephoneReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.

        String callState=intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        if(callState.equals(TelephonyManager.EXTRA_STATE_RINGING)){
            String incomingNumber=intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
            Log.d(TelephoneReceiver.class.getCanonicalName(),"incoming number "+ incomingNumber);
        }

    }
}
