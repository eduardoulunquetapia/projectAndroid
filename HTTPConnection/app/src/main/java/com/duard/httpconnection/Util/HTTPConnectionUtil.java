package com.duard.httpconnection.Util;

import android.os.Message;

import com.duard.httpconnection.HTTPHandler;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by cice on 26/1/17.
 */

public class HTTPConnectionUtil {

    // private static HTTPHandler mHandler;
  /*  private HTTPHandler mHandler;

    public HTTPConnectionUtil(HTTPHandler mHandler) {
        this.mHandler = mHandler;
    }
*/
    public static void obtainResource(final String urlString, final HTTPHandler mHandler) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    URL url = new URL(urlString);
                    URLConnection con = url.openConnection();

                    InputStream in = con.getInputStream();
                    int byteLeidos;
                    while ((byteLeidos = in.read()) != -1) {
//enviar mensaje al handler quien manejara con IU

                        Message m = mHandler.obtainMessage(mHandler.SHOW_DATA, new Integer(byteLeidos));
                        mHandler.sendMessage(m);
                    }


                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();


    }
}
