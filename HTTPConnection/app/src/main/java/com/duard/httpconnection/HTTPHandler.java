package com.duard.httpconnection;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

/**
 * Created by cice on 26/1/17.
 */

public class HTTPHandler extends Handler {


    public static final int SHOW_DATA = 1;
    public static final int SHOW_DATA2 = 2;
    private TextView byteMonitor;

    public HTTPHandler(TextView byteMonitor) {
        this.byteMonitor = byteMonitor;
    }

    // te metodo se crea en menu-code-override
    @Override
    public void handleMessage(Message msg) {


        switch (msg.what) {
            case SHOW_DATA:
                Log.d("Handler", "" + msg.obj);
                byteMonitor.append(msg.obj + "\n");
                break;
            case SHOW_DATA2:

                break;
        }

    }
}
