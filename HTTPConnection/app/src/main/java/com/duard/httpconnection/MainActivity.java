package com.duard.httpconnection;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.duard.httpconnection.Util.HTTPConnectionUtil;

public class MainActivity extends AppCompatActivity {
    public static final String TAG="MainActivity";
    private HTTPHandler mHandler;
    public TextView byteMonitor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        byteMonitor=(TextView)findViewById(R.id.byteMonitorTextView);
        mHandler=new HTTPHandler(byteMonitor);
    }

    public void startDownload(View v){
        Log.d(TAG,"starDownload()...");
        String url="http://www.mit.edu/";
        //creo que es polimorfismo
       // HTTPConnectionUtil  util=new HTTPConnectionUtil(mHandler);
        HTTPConnectionUtil.obtainResource(url, mHandler);



    }
}
