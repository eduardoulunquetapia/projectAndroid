package com.duard.databasetest.database;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by cice on 6/2/17.
 */

public class ArtistOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "artistDB";
    public static final int DATABASE_VERSION = 1;
    private static final String SCHEMA = "CREATE TABLE ARTIST(_id INTEGER PRIMARY KEY AUTOINCREMENT, ARTIST_NAME TEXT)";
    public static final String ARTIST_TABLE="ARTIST";
    public static final String ARTIST_PK="_id";
    public static final String COLUMN_NAME="ARTIST_NAME";
    private int version;
    private Context ctx;

    public ArtistOpenHelper(Context context, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, version);
        this.version = version;
        ctx = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SCHEMA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table artist if exist");
        onCreate(db);
    }
}
