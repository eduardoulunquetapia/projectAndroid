package com.duard.databasetest;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.duard.databasetest.database.ArtistOpenHelper;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    public void insertData(View v) {
        ArtistOpenHelper openHelper = new ArtistOpenHelper(this, null, 1);
        SQLiteDatabase db = openHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ArtistOpenHelper.COLUMN_NAME, "Eduardo Ulunque");
        db.insert(ArtistOpenHelper.ARTIST_TABLE, null, contentValues);
        contentValues.clear();
        contentValues.put(ArtistOpenHelper.COLUMN_NAME, "Helmi Lopez");
        db.insert(ArtistOpenHelper.ARTIST_TABLE, null, contentValues);
        contentValues.clear();
        contentValues.put(ArtistOpenHelper.COLUMN_NAME, "Jose JJ");
        db.insert(ArtistOpenHelper.ARTIST_TABLE, null, contentValues);
        contentValues.clear();
        contentValues.put(ArtistOpenHelper.COLUMN_NAME, "Teresa Tapia");
        db.insert(ArtistOpenHelper.ARTIST_TABLE, null, contentValues);
        db.close();
    }

    public void showData(View v) {
        Intent intent = new Intent(this, ArtistListActivity.class);
        startActivity(intent);
    }
}
