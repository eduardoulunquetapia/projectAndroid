package com.duard.moviedbrestservicetest;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
//https://api.themoviedb.org/3/movie/popular?api_key=ffa1e976c5bd23d97f346f2086d22b11
public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getCanonicalName();
    public static final String URL = "https://api.themoviedb.org/3/movie/";
    public static final String API_KEY = "ffa1e976c5bd23d97f346f2086d22b11";
    private static final String CATEGORY = "popular";
    public static final String ID_MOVIE = "157336/videos";
    List<String> list = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void getMovies(View v) {
        TheMovieDBAsyncTask at = new TheMovieDBAsyncTask();
        at.execute(URL + CATEGORY + "?api_key=" + API_KEY);
    }

    public void getLinkMovies(View v) {
       // TheMovieDBAsyncTask at = new TheMovieDBAsyncTask();
       // list = at.doInBackground(URL + ID_MOVIE + "?api_key=" + API_KEY);
        Intent intent = new Intent(this, Main2Activity.class);
       // intent.putStringArrayListExtra(Main2Activity.EXTRA_KEY, (ArrayList<String>) list);

        startActivity(intent);
    }

     public class TheMovieDBAsyncTask extends AsyncTask<String, Void, List<String>> {

        @Override
        protected List<String> doInBackground(String... urls) {

            BufferedReader in = null;
            List<String> movieList = new ArrayList<>();
            //Retrofit evita tener que gestionar la conexion http
            try {
                URL url = new URL(urls[0]);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                //se va llenando con lo que obtnemos y vamos añadiendo a  data(StringBuffer)
                StringBuffer data = new StringBuffer();
                //insertar los datos obtenidos con in en el StringBuffer
                String line = null;
                while ((line = in.readLine()) != null) {
                    data.append(line);
                }

                JSONObject jsonObject = new JSONObject(data.toString());
                JSONArray results = jsonObject.getJSONArray("results");
                for (int i = 0; i < results.length(); i++) {
                    JSONObject jsonMovie = results.getJSONObject(i);
                    String title = null;
                    if (jsonMovie.isNull("original_title") == false) {
                        title = jsonMovie.getString("original_title");
                        /*Log.d(TAG, " key de videos: " + jsonMovie.isNull("key") );*/
                    } else {
                        title = jsonMovie.getString("key");
                    }
                    /*Log.d(TAG, " titulo de pelicula: " + title);*/
                    movieList.add(title);
                }
                return movieList;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {

                try {
                    if (in != null) in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<String> strings) {
            TextView moviesTextView = (TextView) findViewById(R.id.idMovies);
            for (int i = 0; i <strings.size() ; i++) {
                moviesTextView.setText(strings.get(i));
            }
            /*for (String movie : strings) {
                moviesTextView.setText(movie.toString());

            /*for (int i = 0; i < strings.size(); i++) {
               *//* Log.d(TAG, "titulo movie: " + strings.get(i).toString());*//*
                moviesTextView.append("\n" + strings.get(i).toString());
            }*/
            for (String movie : strings) {
                moviesTextView.append("\n" + movie.toString());
            }
        }
    }
}
