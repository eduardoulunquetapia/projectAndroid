package com.duard.moviedbrestservicetest;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by duard_mac on 6/2/17.
 */

public class MovieDBAsyncTask extends AsyncTask<String, Void, List<String>> {
    private static final String EXTRA_KEY = "key";
    private static final String URL_YOUTUBE = "https://www.youtube.com/watch?v=";
    public Button keyTextView;

    public TextView getKeyTextView() {
        return keyTextView;
    }

    public void setKeyTextView(Button keyTextView) {
        this.keyTextView = keyTextView;
    }

    @Override
    public List<String> doInBackground(String... urls) {

        BufferedReader in = null;
        List<String> movieList = new ArrayList<>();
        //Retrofit evita tener que gestionar la conexion http
        try {
            URL url = new URL(urls[0]);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            //se va llenando con lo que obtnemos y vamos añadiendo a  data(StringBuffer)
            StringBuffer data = new StringBuffer();
            //insertar los datos obtenidos con in en el StringBuffer
            String line = null;
            while ((line = in.readLine()) != null) {
                data.append(line);
            }

            JSONObject jsonObject = new JSONObject(data.toString());
            JSONArray results = jsonObject.getJSONArray("results");
            for (int i = 0; i < results.length(); i++) {
                JSONObject jsonMovie = results.getJSONObject(i);
                String title = null;
                if (jsonMovie.isNull("original_title") == false) {
                    title = jsonMovie.getString("original_title");
                        /*Log.d(TAG, " key de videos: " + jsonMovie.isNull("key") );*/
                } else {
                    title = jsonMovie.getString("key");
                }
                    /*Log.d(TAG, " titulo de pelicula: " + title);*/
                movieList.add(title);
            }
            return movieList;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {

            try {
                if (in != null) in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(final List<String> strings) {

        /*for (int i = 0; i < strings.size(); i++) {
            final int finalI = i;
            keyTextView.setText("\n" + URL_YOUTUBE + strings.get(finalI).toString());
            keyTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   *//* Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse(URL_YOUTUBE + strings.get(finalI).toString()));
                    startActivity(intent);*//*
                    // Log.d(TAG, strings.get(finalI).toString());
                }
            });
        }*/
    }


}
