package com.duard.moviedbrestservicetest;

import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class Main2Activity extends AppCompatActivity {
    public static final String ID_MOVIE = "157336/videos";
    private static final String YOUTUBE = "www.youtube.com/watch?v=";
    private static final String URL_YOUTUBE = "https://www.youtube.com/watch?v=";
    public static final String EXTRA_KEY = "key";
    private static final String TAG = "MAIN 2";
    public static Intent intent;
    public ListView listView;
    private List<String> list;
    private AdapterView.OnItemClickListener mMsgClickHandler;
    MovieDBAsyncTask mat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        listView = (ListView) findViewById(R.id.idListView);
        String url = MainActivity.URL + MainActivity.ID_MOVIE + "?api_key=" + MainActivity.API_KEY;

        mat = new MovieDBAsyncTask();
        mat.execute(url);
        ListAdapter adapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                getListLink()
        );
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> pariente, View view, int posicion, long id) {
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(URL_YOUTUBE + pariente.getItemAtPosition(posicion).toString()));
                startActivity(intent);/*
                Toast toast = Toast.makeText(Main2Activity.this, YOUTUBE+pariente.getItemAtPosition(posicion).toString(), Toast.LENGTH_LONG);
                toast.show();*/
            }
        });
    }

    private List<String> getListLink() {
        try {
            return list = mat.get();
         /*   for (final String link :
                    list) {
                listView1.setText(YOUTUBE+link);
                listView1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(URL_YOUTUBE + link));
                        startActivity(intent);
                        Log.d(TAG, " enlace " + link);
                    }
                });

            }*/
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }
}

