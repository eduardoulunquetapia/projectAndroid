package com.duard.mapgoogle.adapters;

import android.app.Activity;
import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.duard.mapgoogle.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.util.zip.Inflater;

import static com.duard.mapgoogle.R.layout.tag_marker;

/**
 * Created by cice on 10/2/17.
 */

public class InfoAdapter implements GoogleMap.InfoWindowAdapter {

    private static String title;
    private static String TEXT = "Este espacion esta reservado para la descripcion";
    private static String lat;
    private static String lon;
    private static Image image;
    LayoutInflater layoutInflater;
    TextView latTextView;
    TextView lonTextView;
    TextView titleTextView;
    TextView textTextView;
    ImageView imageView;
    Activity activity;

    public InfoAdapter(Activity activity) {
        this.activity = (Activity) activity;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        lat = String.valueOf(marker.getPosition().latitude);
        lon = String.valueOf(marker.getPosition().longitude);
        title = marker.getTitle();
        View view = activity.getLayoutInflater().inflate(R.layout.tag_marker, null);
        latTextView = (TextView) view.findViewById(R.id.idLatitud);
        latTextView.setText(lat);
        lonTextView = (TextView) view.findViewById(R.id.idLongitud);
        lonTextView.setText(lat);
        textTextView = (TextView) view.findViewById(R.id.idText);
        textTextView.setText(TEXT);
        titleTextView = (TextView) view.findViewById(R.id.idTitle);
        titleTextView.setText(title);
        imageView = (ImageView) view.findViewById(R.id.idImageView);
        imageView.setImageResource(R.drawable.jellybean);


        return view;
    }

    @Override
    public View getInfoContents(Marker marker) {

        return null;
    }
}
