package com.duard.mapgoogle;

import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.ZoomButton;
import android.widget.ZoomControls;

import com.duard.mapgoogle.adapters.InfoAdapter;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    private InfoAdapter infoAdapter;
    private GoogleMap mMap;
    private UiSettings btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        //  btn=new UiSettings.setZoomControlsEnabled(true);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {


        mMap = googleMap;

        infoAdapter = new InfoAdapter(this);
        mMap.setInfoWindowAdapter(infoAdapter);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        LatLng madrid = new LatLng(40.417325, -3.6836);
        CameraPosition madridPos = new CameraPosition.Builder()
                .target(madrid)
                .zoom(16)
                //.bearing(90)
                // .tilt(90)
                .build();
        mMap.addMarker(new MarkerOptions().position(madrid).title("Que Viva España"));
        CameraUpdate madridUp = CameraUpdateFactory.newCameraPosition(madridPos);
        mMap.animateCamera(madridUp);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                Marker marker = mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title("market")
                        .snippet("olita"));
                marker.setDraggable(true);
                mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title("instituto")
                        .snippet("madre Mia"));
            }
        });
       /* mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                                return true;
            }
        });
*/
        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {
                mMap.addMarker(new MarkerOptions().position(marker.getPosition()));
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {

            }
        });

    }
}
