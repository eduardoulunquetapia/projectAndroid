package com.duard.customreceiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.duard.customreceiver.receiver.CustomAlarmReceiver;
import com.duard.customreceiver.receiver.CustomEventReceiver;

public class MainActivity extends AppCompatActivity {
    private CustomEventReceiver customEventReceiver;
    private EditText alarmEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        customEventReceiver = new CustomEventReceiver();
        alarmEditText = (EditText) findViewById(R.id.idEditText);
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter(CustomEventReceiver.CUSTOM_EVENT);
        registerReceiver(customEventReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(customEventReceiver);
    }

    public void sendButton1(View view) {
        Intent intent = new Intent(CustomEventReceiver.CUSTOM_EVENT);
        sendBroadcast(intent);
    }

    public void sendButton2(View view) {
        Intent intent = new Intent(this, CustomAlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1000, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        int elapsedTime = Integer.parseInt(alarmEditText.getText().toString());
        alarmManager.set(
                AlarmManager.RTC_WAKEUP
                , System.currentTimeMillis() + elapsedTime * 1000
                , pendingIntent);
    }
}
