package com.duard.customreceiver.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class CustomEventReceiver extends BroadcastReceiver {
    //usamos el nombre de MYEVENT para propagar el evento
    public static final String CUSTOM_EVENT="com.duard.customreceiver.MYEVENT";
    public CustomEventReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        Toast.makeText(context, "received MYEVENT",Toast.LENGTH_LONG).show();
    }
}
