package com.duard.customreceiver.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class CustomAlarmReceiver extends BroadcastReceiver {
    public CustomAlarmReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        Toast.makeText(context, "received CUSTOM_ALARM_RECEIVER",Toast.LENGTH_LONG).show();
    }
}
