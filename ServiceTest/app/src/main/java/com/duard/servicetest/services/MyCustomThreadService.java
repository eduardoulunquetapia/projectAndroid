package com.duard.servicetest.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.duard.servicetest.R;
import com.duard.servicetest.TestNotificationActivity;

public class MyCustomThreadService extends Service {

    private static final String HANDLER_THREAD_NAME = "MyCustomThreadService";
    private MyCustomHandler mHandler;
    public static final int CUSTOM_WHAT = 1;
    private static final  String TAG=MyCustomThreadService.class.getName();

    public MyCustomThreadService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Message msg = mHandler.obtainMessage(CUSTOM_WHAT);
        Log.d(TAG,"ID del Thread: "+Thread.currentThread().getId()+" onStartCommand()...");
        mHandler.sendMessage(msg);
        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //creamos un hilo HandlerThread
        HandlerThread mThread = new HandlerThread(HANDLER_THREAD_NAME);
        mThread.start();
        //añadimos msg en la cola
        //el handler(looper) tambien no ayuda a obtener msg libres

        //construyo con el looper asociado al hilo
        mHandler = new MyCustomHandler(mThread.getLooper());
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public class MyCustomHandler extends Handler {

        public MyCustomHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case CUSTOM_WHAT:
                Log.d(TAG,"ID del Thread: "+Thread.currentThread().getId()+ " HandlerMessages()...");
                for (int i = 0; i < 10; i++) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Log.d(TAG, "ID del Thread: "+Thread.currentThread().getId()+" myCustomThreadService is working...");
                }
                Intent i=new Intent(getApplicationContext(), TestNotificationActivity.class);
                //..getActivity(..,codigo de peticion,i,0)
                PendingIntent pIntent=PendingIntent.getActivity(getApplicationContext(),1,i,0);
                //creamos una notificacion
                Notification.Builder builer = new Notification.Builder(getApplicationContext());
                //creamo una factoria
                builer
                        .setSmallIcon(R.drawable.ic_notification_service)
                        .setContentTitle("testService")
                        .setContentText("test service finished")
                        .setContentIntent(pIntent);
                //creamos la Notificacion
                Notification notificacion = builer.build();
                NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                //.notify(num de notificacion, class Notification)
                nm.notify(100, notificacion);
                break;

            }
        }
    }

}
