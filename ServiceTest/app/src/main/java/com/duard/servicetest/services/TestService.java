package com.duard.servicetest.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.duard.servicetest.R;
import com.duard.servicetest.TestNotificationActivity;

public class TestService extends Service {
    private String TAG = "TestService";

    public TestService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d(TAG, "ID del Thread: "+Thread.currentThread().getId()+"onStartCommand...");
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "TestService ejecutandose jijijijijiiii...");
        }
        Intent i=new Intent(getApplicationContext(), TestNotificationActivity.class);
        //..getActivity(..,codigo de peticion,i,0)
        PendingIntent pIntent=PendingIntent.getActivity(getApplicationContext(),1,i,0);
        //creamos una notificacion
        Notification.Builder builer = new Notification.Builder(this);
        //creamo una factoria
        builer
                .setSmallIcon(R.drawable.ic_notification_service)
                .setContentTitle("testService")
                .setContentText("test service finished")
                .setContentIntent(pIntent);
        //creamos la Notificacion
        Notification notificacion = builer.build();
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //.notify(num de notificacion, class Notification)
        nm.notify(100, notificacion);
        //si lo matas no hace que lo reanudes START_NOT_STICKY
        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
