package com.duard.servicetest.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

import com.duard.servicetest.R;
import com.duard.servicetest.TestNotificationActivity;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class TestIntentService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_FOO = "com.duard.servicetest.services.action.FOO";
    private static final String ACTION_BAZ = "com.duard.servicetest.services.action.BAZ";

    // TODO: Rename parameters
    public static final String EXTRA_PARAM1 = "com.duard.servicetest.services.extra.PARAM1";
    public static final String EXTRA_PARAM2 = "com.duard.servicetest.services.extra.PARAM2";
    public static final String TAG = "IntentService";

    public TestIntentService() {
        super("TestIntentService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionFoo(Context context, String param1, String param2) {
        Intent intent = new Intent(context, TestIntentService.class);
        intent.setAction(ACTION_FOO);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionBaz(Context context, String param1, String param2) {
        Intent intent = new Intent(context, TestIntentService.class);
        intent.setAction(ACTION_BAZ);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    // este metodo seria como onStartCommand de Service
    @Override
    protected void onHandleIntent(Intent intent) {
        String extra1=intent.getStringExtra(EXTRA_PARAM1);

        Log.d(TAG, "ID del Thread: "+Thread.currentThread().getId()+" onHandleIntent...");
        Log.d(TAG, EXTRA_PARAM1+":"+extra1);
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "TestIntentService ejecutandose jujujuuuu..");
        }
        //PendingIntent envuelve a un Intent es otro Intent
        Intent i = new Intent(getApplicationContext(), TestNotificationActivity.class);
        //..getActivity(..,codigo de peticion,i,0)
        PendingIntent pIntent = PendingIntent.getActivity(getApplicationContext(), 1, i, 0);
        //creamos una notificacion
        Notification.Builder builer = new Notification.Builder(this);
        //creamo una factoria
        builer
                .setSmallIcon(R.drawable.ic_test_notification)
                .setContentTitle("testIntentService")
                .setContentText("test intent service finished")
                .setContentIntent(pIntent);
        //creamos la Notificacion
        Notification notificacion = builer.build();
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //.notify(num de notificacion, class Notification)
        nm.notify(100, notificacion);

    }
    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFoo(String param1, String param2) {
        // TODO: Handle action Foo
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionBaz(String param1, String param2) {
        // TODO: Handle action Baz
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
