package com.duard.cookiegame.moster;

import com.duard.cookiegame.cookieCup.SingletonCookieCup;

import java.util.Random;

/**
 * Created by cice on 31/1/17.
 */

public class Moster {
    int numberCookie;
    int timeEating;

    public Moster() {
    }

    public Moster(int numberCookie, int timeEating) {
        this.numberCookie = numberCookie;
        this.timeEating = timeEating;
    }


    public int getNumberCookie() {
        return numberCookie;
    }

    public void setNumberCookie(int numberCookie) {

        this.numberCookie = this.numberCookie+numberCookie;
    }

    public int getTimeEating() {
        timeEating=new Random().nextInt(5);
        return timeEating;
    }

    public void setTimeEating(int timeEating) {
        this.timeEating = timeEating;
    }
}
