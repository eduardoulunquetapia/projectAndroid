package com.duard.cookiegame;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.duard.cookiegame.moster.Moster;

public class MainActivity extends AppCompatActivity {
    private static Button btnStart, btnCancel;
    private static TextView numberCookieTV1, numberCookieTV2, numberCookieOvenTV1, timeRunTV;
    private static ProgressBar monsterPB1, monsterPB2, ovenPB;
    private static AsyncCookies contadorCookies1,contadorCookies2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnStart = (Button) findViewById(R.id.btnStart);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        contadorCookies1 = new AsyncCookies();
        contadorCookies2 = new AsyncCookies();

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPlay();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelPlay();
            }

        });
    }

    private void startPlay() {
        contadorCookies1.execute(new Moster());
        contadorCookies2.execute(new Moster());
    }

    private void cancelPlay() {

    }

}
