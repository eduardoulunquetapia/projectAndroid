package com.duard.cookiegame.cookieCup;

/**
 * Created by duard_mac on 5/2/17.
 */
public class SingletonCupCookies {
    private static SingletonCupCookies ourInstance = new SingletonCupCookies();
    public int cookies;
    public int getCookies() {
        return cookies;
    }

    public void setCookies(int cookies) {
        this.cookies = this.cookies+cookies;
    }



    public static SingletonCupCookies getInstance() {
        return ourInstance;
    }

    public SingletonCupCookies() {
    }
}
