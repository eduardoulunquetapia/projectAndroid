package com.duard.cookiegame.cookieOven;

/**
 * Created by cice on 31/1/17.
 */

public class CookieOven {

    int timeCookieBaked = 5;
    int plusCookieBaked=10;

    public int getTimeCookieBaked() {
        return timeCookieBaked;
    }

    public void setTimeCookieBaked(int timeCookieBaked) {
        this.timeCookieBaked = timeCookieBaked;
    }


    public int getPlusCookie() {
        return plusCookieBaked;
    }

    public void setPlusCookie(int plusCookie) {
        this.plusCookieBaked = plusCookie;
    }


}

