package com.duard.dynamicfragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.duard.dynamicfragment.fragments.QuoteFragment;
import com.duard.dynamicfragment.fragments.TitleListFragment;

public class MainActivity extends AppCompatActivity implements TitleListFragment.titlesListFragmentHostingActivity {
    private QuoteFragment qFragment;
    private TitleListFragment litleFragment;
    private FrameLayout titleContainer, quoteContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        super.onCreate(savedInstanceState);
        titleContainer=(FrameLayout)findViewById(R.id.titleContainer);
        quoteContainer=(FrameLayout)findViewById(R.id.quoteContainer);
        qFragment=new QuoteFragment();
        litleFragment=new TitleListFragment();
        FragmentManager fm= getSupportFragmentManager();
        fm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                //creamos metodo setLayout()
                setLayout();
            }
        });
        FragmentTransaction ft=fm.beginTransaction();
        ft.add(R.id.titleContainer,litleFragment);
        ft.addToBackStack("composicion inicial");
        ft.commit();

        fm.executePendingTransactions();
    }

    @Override
    public void showTitle(int index) {
        if(!qFragment.isAdded()){
            FragmentManager fm= getSupportFragmentManager();
            FragmentTransaction ft =fm.beginTransaction();
            ft.add(R.id.quoteContainer,qFragment);
            ft.addToBackStack(null);
            ft.commit();
            fm.executePendingTransactions();
        }
        qFragment.showTitle(index);
    }
    public void setLayout(){
        if(qFragment.isAdded()){
            titleContainer.setLayoutParams(new LinearLayout.LayoutParams(0,
                    ViewGroup.LayoutParams.MATCH_PARENT,1f));
            quoteContainer.setLayoutParams(new LinearLayout.LayoutParams(0,
                    ViewGroup.LayoutParams.MATCH_PARENT,2f));
      }else{
            titleContainer.setLayoutParams(new LinearLayout.LayoutParams(0,
                    ViewGroup.LayoutParams.MATCH_PARENT,1f));
            quoteContainer.setLayoutParams(new LinearLayout.LayoutParams(0,
                    ViewGroup.LayoutParams.MATCH_PARENT,0f));

        }
    }
}
