package com.duard.dynamicfragment.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.duard.dynamicfragment.R;

public class QuoteFragment extends Fragment {

    private Context ctx;
    public static final String TAG="qouteFragment";
    private TextView titleTextView, quoteTextView;
    private String[] quotes;
    private String[] titles;
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG,"onActivityCreated()...");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG,"onPause()...");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG,"onStop()...");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG,"onStart()...");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG,"onResume()...");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.ctx=context;
        quotes=ctx.getResources().getStringArray(R.array.citas);
        titles=ctx.getResources().getStringArray(R.array.titles);

        Log.d(TAG,"onAttach()...");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"onCreate()...");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"onDestroy()...");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout=inflater.inflate(R.layout.fragment_qoute, container, false);
        quoteTextView=(TextView) layout.findViewById(R.id.quoteTextView);
        titleTextView=(TextView)layout.findViewById(R.id.titleTextView);
        Log.d(TAG,"onCreateView()...");

        return layout;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(TAG,"onDetach()...");
    }

    public void showTitle(int index) {
        titleTextView.setText(titles[index]);
        quoteTextView.setText(quotes[index]);
    }
}
