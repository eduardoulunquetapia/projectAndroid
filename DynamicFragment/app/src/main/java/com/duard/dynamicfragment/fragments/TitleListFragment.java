package com.duard.dynamicfragment.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.duard.dynamicfragment.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TitleListFragment extends Fragment {
    private Context ctx;
    private ListView titlesListView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof  titlesListFragmentHostingActivity)
        ctx=context;
        //sino producir una excepcion
    }

    public TitleListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View layout =inflater.inflate(R.layout.fragment_title_list,container,false);
        titlesListView=(ListView) layout.findViewById(R.id.titlesListView);
        String[] titles= ctx.getResources().getStringArray(R.array.titles);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1,android.R.id.text1,titles);
        titlesListView.setAdapter(adapter);
        titlesListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ((titlesListFragmentHostingActivity)ctx).showTitle(i);
            }
        });

        return layout;
    }

    //interfaz define los requisitos de counucacion de TitlesLisTFragment
    //con la actividad que lo aloja
    public interface titlesListFragmentHostingActivity{

        public void showTitle(int index);
    }
}
