package com.duard.webviewtest;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.webkit.JavascriptInterface;

/**
 * Created by cice on 14/2/17.
 */

public class AlertToast {
    Context context;

    public AlertToast(Context context) {
        this.context = context;
    }

    @JavascriptInterface
    public void showDialog(String msn) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this.context);
        builder.setMessage(msn).setNeutralButton("OK.....mensaje"
                , new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }

}
