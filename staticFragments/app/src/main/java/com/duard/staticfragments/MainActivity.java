package com.duard.staticfragments;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TableLayout;

import com.duard.staticfragments.Fragments.QuoteFragment;
import com.duard.staticfragments.Fragments.TitleListFragment;

public class MainActivity extends AppCompatActivity implements TitleListFragment.titlesListFragmentHostingActivity {

    private QuoteFragment qFragment;
    private int orientacion;
    public static final String TAG="MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager fm=getSupportFragmentManager();
        qFragment=(QuoteFragment)fm.findFragmentById(R.id.qouteFragments);
        orientacion=getResources().getConfiguration().orientation;
        Log.d(TAG,"onCreate()..."+orientacion);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        orientacion=newConfig.orientation;
        Log.d(TAG,"onconfigurationChanged().....orientacion: "+orientacion);

    }

    @Override
    public void showTitle(int index) {

        if(orientacion==Configuration.ORIENTATION_LANDSCAPE){
            if(qFragment!=null)
                qFragment.showTitle(index);
        }else{
            Intent intent=new Intent(this, QuoteActivity.class);
            intent.putExtra(QuoteActivity.QUOTE_INDEX, index);
            startActivity(intent);
        }

    }
}
