package com.duard.staticfragments;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.duard.staticfragments.Fragments.QuoteFragment;
import com.duard.staticfragments.R;

public class QuoteActivity extends AppCompatActivity {
    public static final String QUOTE_INDEX="index";
   // private String [] quotes;
   // private String [] titles;
 private QuoteFragment qFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quote);

        qFragment=(QuoteFragment) getSupportFragmentManager().findFragmentById(R.id.qouteFragment2);
        Intent intent=getIntent();
        int index=intent.getIntExtra(QUOTE_INDEX,-1);
       // Intent.getIntExtra(QUOTE_INDEX,-1);
        qFragment.showTitle(index);

       // quotes=getResources().getStringArray(R.array.citas);
      //  titles=getResources().getStringArray(R.array.titles);
    }
}
