package cice.pdfexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindColor;
import butterknife.BindDimen;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Main2Activity extends AppCompatActivity {

    @BindView(R2.id.button2) Button button2;
    @BindView(R2.id.editText) EditText editText;
    @BindView(R2.id.textView) TextView textView;

    @BindString(R2.string.app_name) String nombreApp;
    @BindColor(R2.color.colorAccent) int color;
    @BindDimen(R2.dimen.activity_horizontal_margin) float margenHorizontal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        ButterKnife.bind(this);
        int i =0;

    }

    @OnClick(R.id.button2)
    public void clickButton(View v){
        int i = 0;
        String s = "3";
    }
}
