package com.duard.dialogtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.duard.dialogtest.dialogs.MyCustomLayoutDialog;
import com.duard.dialogtest.dialogs.MyPositiveNegativeDialog;

import java.util.Map;

public class MainActivity extends AppCompatActivity implements MyCustomLayoutDialog.CustomDialogInterface {

    private TextView monitor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        monitor = (TextView) findViewById(R.id.monitor);
    }

    public void showPositiveNegativeDialog(View v) {
        MyPositiveNegativeDialog myDialog = new MyPositiveNegativeDialog();
        myDialog.show(getFragmentManager(), "myPositiveNegativeDialog");
    }

    public void showCustomLayoutDialog(View v) {
        MyCustomLayoutDialog myDialog = new MyCustomLayoutDialog();
        myDialog.show(getFragmentManager(), "myCustomLayoutDialog");
    }

    @Override
    public void setData(Map<String, String> data) {
        String email = data.get(MyCustomLayoutDialog.EMAIL_KEY);
        String name = data.get(MyCustomLayoutDialog.NAME_KEY);
        monitor.setText("name: " + name + "\nemail: " + email);
    }
}
