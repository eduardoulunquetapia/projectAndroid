package com.duard.dialogtest.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.duard.dialogtest.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by cice on 25/1/17.
 */

public class MyCustomLayoutDialog extends DialogFragment {
    private CustomDialogInterface mDialogInterface;
    public static final String EMAIL_KEY = "email";
    public static final String NAME_KEY = "name";
    private EditText nameET, emailET;

    public interface CustomDialogInterface {
        public void setData(Map<String, String> data);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mDialogInterface = (CustomDialogInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("el contexto debe implentar el interfaz CustomDialogInterface");
        }

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View layout = getActivity().getLayoutInflater().inflate(R.layout.custom_dialog_layout, null);
        nameET = (EditText) layout.findViewById(R.id.nombreEditText);
        emailET = (EditText) layout.findViewById(R.id.emailEditText);
        AlertDialog.Builder builer = new AlertDialog.Builder(getActivity());
        builer.setMessage("Custom layout Dialog").setPositiveButton("ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Map<String, String> data = new HashMap<String, String>();
                data.put(NAME_KEY, nameET.getText().toString());
                data.put(EMAIL_KEY, emailET.getText().toString());
                mDialogInterface.setData(data);

                //Toast.makeText(getActivity(), "OK", Toast.LENGTH_LONG).show();
            }
        }).setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //Toast.makeText(getActivity(), "Cancel", Toast.LENGTH_LONG).show();
            }
        }).setView(layout);

        return builer.create();

    }
}

