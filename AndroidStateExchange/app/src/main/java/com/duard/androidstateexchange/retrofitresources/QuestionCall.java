package com.duard.androidstateexchange.retrofitresources;

import com.duard.androidstateexchange.model.Item;
import com.duard.androidstateexchange.model.QuestionGroup;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by cice on 7/2/17. https://api.stackexchange.com/2.1/questions?order=desc&sort=creation&site=stackoverflow
 */

public interface QuestionCall {
    @GET("questions?order=desc&sort=creation&site=stackoverflow")
    public Call<QuestionGroup> getQuestionCall(@Query("tagged") String filtro);


}
