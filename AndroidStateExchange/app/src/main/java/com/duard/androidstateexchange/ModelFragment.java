package com.duard.androidstateexchange;


import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.duard.androidstateexchange.database.QuestionsOpenHelperSingleton;
import com.duard.androidstateexchange.events.NewDataEvent;
import com.duard.androidstateexchange.model.Item;
import com.duard.androidstateexchange.model.QuestionGroup;
import com.duard.androidstateexchange.retrofitresources.QuestionCall;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class ModelFragment extends Fragment {


    public ModelFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new ModelLoadThread().start();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        TextView textView = new TextView(getActivity());
        textView.setText(R.string.hello_blank_fragment);
        return textView;
    }

    public class ModelLoadThread extends Thread {
        public void run() {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(("https://api.stackexchange.com/2.1/"))
                    .addConverterFactory(GsonConverterFactory.create())//conversor a json
                    .build();
            QuestionCall service = retrofit.create(QuestionCall.class);
            try {
                QuestionGroup questionGroup = service.getQuestionCall(".net").execute().body();
                QuestionsOpenHelperSingleton questionOpenHelper =
                        QuestionsOpenHelperSingleton.getInstance(getActivity());
                Cursor cursor = questionOpenHelper.insert(questionGroup.items);
                EventBus.getDefault().postSticky(new NewDataEvent(cursor));
            } catch (IOException e) {
                e.printStackTrace();
            }

         /*   for (int i = 0; i <10 ; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                EventBus.getDefault().postSticky(new NewDataEvent());
            }*/
        }
    }
}
