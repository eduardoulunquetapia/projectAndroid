package com.duard.androidstateexchange;


import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.duard.androidstateexchange.database.QuestionsOpenHelperSingleton;
import com.duard.androidstateexchange.events.NewDataEvent;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * A simple {@link Fragment} subclass.
 */
public class QuestionListFragment extends ListFragment {
    private static final String Tag = QuestionListFragment.class.getCanonicalName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(
                getActivity()
                , R.layout.question_row
                , null
                , new String[]{QuestionsOpenHelperSingleton.OWNER_AVATAR_COLUMN, QuestionsOpenHelperSingleton.QUESTION_TITLE_COLUMN}
                , new int[]{R.id.idImageView, R.id.idTextView}
                , 0);
        simpleCursorAdapter.setViewBinder(new QuestionViewBinder());
        setListAdapter(simpleCursorAdapter);


        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public QuestionListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getDataNotificationEvent(NewDataEvent event) {
        Cursor cursor = event.getCursor();
        ((CursorAdapter) getListView().getAdapter()).swapCursor(cursor);
        ((CursorAdapter) getListView().getAdapter()).notifyDataSetChanged();
        Log.d(Tag, "getDatanotificationEvent()...");
    }


    private class QuestionViewBinder implements SimpleCursorAdapter.ViewBinder {
        @Override
        public boolean setViewValue(View view, Cursor cursor, int columnIndex) {

            switch (view.getId()) {
                case R.id.idImageView:
                    Picasso.with(getActivity())
                            .load(Uri.parse(cursor.getString(cursor.getColumnIndex(
                                    QuestionsOpenHelperSingleton.OWNER_AVATAR_COLUMN))))
                            .resize(100, 100)
                            .centerCrop()
                            .into((ImageView) view);
                    return true;
                case R.id.idTextView:
                    ((TextView) view).setText(
                            cursor.getString(
                                    cursor.getColumnIndex(
                                            QuestionsOpenHelperSingleton.QUESTION_TITLE_COLUMN)));
                    return true;
            }
            return false;
        }
    }
}
