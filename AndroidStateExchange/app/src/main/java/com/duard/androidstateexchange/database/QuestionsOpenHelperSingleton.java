package com.duard.androidstateexchange.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.duard.androidstateexchange.model.Item;

import java.util.List;

/**
 * Created by cice on 7/2/17.
 */

public class QuestionsOpenHelperSingleton extends SQLiteOpenHelper {
    public static final String PK_COLUMN = "_id";
    public static final String OWNER_AVATAR_COLUMN = "PROFILE_NAME";
    public static final String QUESTION_TITLE_COLUMN = "TITLE";
    public static final String QUESTION_ID_COLUMN = "QUESTION_ID";
    public static final String QUESTION_LINK_COLUMN = "LINK";
    public static final String OWNER_ID_COLUMN = "USER_ID";
    public static final String QUESTION_TABLE = "questions";

    public static final String QUESTIONS_DB = "questionsDB";
    public static final int VERSION = 1;
    //referencia del singleton
    private static QuestionsOpenHelperSingleton questionsOpenHelper;
    private static Context ctx;

    private QuestionsOpenHelperSingleton(Context context) {
        super(context, QUESTIONS_DB, null, VERSION);
    }

    public static QuestionsOpenHelperSingleton getInstance(Context ctx) {
        if (questionsOpenHelper == null) {
            questionsOpenHelper = new QuestionsOpenHelperSingleton(ctx);
            QuestionsOpenHelperSingleton.ctx = ctx;
        }
        return questionsOpenHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table questions("
                    + PK_COLUMN+" integer primary key autoincrement " +
                "," + QUESTION_ID_COLUMN + " integer" +
                "," + QUESTION_TITLE_COLUMN + " text" +
                "," + QUESTION_LINK_COLUMN + " text" +
                "," + OWNER_ID_COLUMN + " integer" +
                "," + OWNER_AVATAR_COLUMN + " integer)";
        db.execSQL(sql);

    }

    public Cursor insert(List<Item> questions) {
        String sql = "insert into " +
                QUESTION_TABLE + "(" +
                QUESTION_ID_COLUMN + "," +
                QUESTION_TITLE_COLUMN + "," +
                QUESTION_LINK_COLUMN + "," +
                OWNER_ID_COLUMN + "," +
                OWNER_AVATAR_COLUMN + ") values(?,?,?,?,?)";
        //questionsOpenHelper es referencia del singleton
        SQLiteDatabase db = questionsOpenHelper.getWritableDatabase();
        db.beginTransaction();
        for (Item item : questions) {
            db.execSQL(sql
                    , new Object[]{item.questionId
                            , item.title
                            , item.link
                            , item.owner.userId
                            , item.owner.profileImage});

        }
        db.setTransactionSuccessful();
        db.endTransaction();
        return db.query(QUESTION_TABLE, new String[]{PK_COLUMN, QUESTION_TITLE_COLUMN, OWNER_AVATAR_COLUMN},
                null, null, null, null, null);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        throw new RuntimeException("no es admitido la actualizacion de esta base de datos");
    }
}
