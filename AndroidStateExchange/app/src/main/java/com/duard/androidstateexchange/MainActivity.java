package com.duard.androidstateexchange;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    private static final String MODEL_FRAGMENT_TAG = "Model Fragment";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        QuestionListFragment questionListFragment = new QuestionListFragment();
        fragmentTransaction.add(R.id.fragmentContainer, questionListFragment, null);
        ModelFragment modelFragment = new ModelFragment();
        fragmentTransaction.add(modelFragment, MODEL_FRAGMENT_TAG);
        fragmentTransaction.commit();
        getSupportFragmentManager().executePendingTransactions();
    }
}
