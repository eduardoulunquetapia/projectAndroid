package com.duard.carlistview.model;

/**
 * Created by cice on 19/1/17.
 */

public class Car {
    public Car(String descripcion, String fabricante, int image, String modelo, int thumb) {
        this.descripcion = descripcion;
        this.fabricante = fabricante;
        this.image = image;
        this.modelo = modelo;
        this.thumb = thumb;
    }

    private String modelo;
    private String fabricante;
    private  String descripcion;
    private int thumb;
    private int image;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getThumb() {
        return thumb;
    }

    public void setThumb(int thumb) {
        this.thumb = thumb;
    }
}

