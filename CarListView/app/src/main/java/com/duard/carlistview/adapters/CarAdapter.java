package com.duard.carlistview.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.duard.carlistview.R;
import com.duard.carlistview.model.Car;

import java.util.List;

/**
 * Created by cice on 19/1/17.
 */

public class CarAdapter extends ArrayAdapter<Car>{
    private Context ctx;
    private  List<Car> dataCar;
    public CarAdapter(Context context, int resource, List<Car> objects) {
        super(context, resource, objects);
        ctx=context;
        dataCar=objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater=LayoutInflater.from(ctx);
        View row=inflater.inflate(R.layout.car_row,null);
        ImageView carImageView=(ImageView) row.findViewById(R.id.carThumbIdView);
        TextView carTextView =(TextView) row.findViewById(R.id.carNameTextView);
        carImageView.setImageResource(dataCar.get(position).getThumb());
        carTextView.setText(dataCar.get(position).getFabricante()+" "+dataCar.get(position).getModelo());
        return row;
    }
}
