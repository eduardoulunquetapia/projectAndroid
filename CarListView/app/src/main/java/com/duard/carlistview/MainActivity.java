package com.duard.carlistview;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.duard.carlistview.adapters.CarAdapter;
import com.duard.carlistview.model.Car;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private  String TAG="MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<Car> data= (List<Car>) getData();
        CarAdapter adapter=new CarAdapter(this, R.layout.car_row,data);
        ListView carListView=(ListView) findViewById(R.id.idCarView);
        carListView.setAdapter(adapter);
        carListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){


            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG,"click en item"+position+"...");
                Toast.makeText(MainActivity.this,"click en item ..."+position,Toast.LENGTH_LONG).show();
            }
        });

    }
    private List<Car> getData(){
        List<Car> list =new ArrayList<>();
        list.add(new Car("bla bla bla", "peugot", R.drawable.car1,"307",R.drawable.car1_thumbnail));
        list.add(new Car("bla bla bla", "nissa", R.drawable.car2,"307",R.drawable.car2_thumbnail));
        list.add(new Car("bla bla bla", "honda", R.drawable.car3,"307",R.drawable.car3_thumbnail));
        list.add(new Car("bla bla bla", "volvo", R.drawable.car4,"307",R.drawable.car4_thumbnail));
        list.add(new Car("bla bla bla", "opel", R.drawable.car5,"307",R.drawable.car5_thumbnail));
        list.add(new Car("bla bla bla", "mazda", R.drawable.car6,"307",R.drawable.car6_thumbnail));
        list.add(new Car("bla bla bla", "toyota", R.drawable.car7,"307",R.drawable.car7_thumbnail));



        return list;
    }
}
