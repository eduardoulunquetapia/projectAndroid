package com.duard.projectandroid.threads;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.duard.projectandroid.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by cice on 15/2/17.
 */

public class AsyncTaskCategory extends AsyncTask<String, Void, List<String>> {
    private ArrayAdapter<String> spinnerArrayAdapter;

    public AsyncTaskCategory(ArrayAdapter<String> spinnerArrayAdapter) {
        this.spinnerArrayAdapter = spinnerArrayAdapter;
    }

    @Override
    protected void onPostExecute(List<String> strings) {
        Collections.sort(strings);
        spinnerArrayAdapter.addAll(strings);
        spinnerArrayAdapter.notifyDataSetChanged();
    }

    @Override
    protected List<String> doInBackground(String... params) {
        BufferedReader in = null;
        List<String> listCategory = new ArrayList<>();
        try {
            URL url = new URL(params[0]);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            //se va llenando con lo que obtnemos y vamos añadiendo a  data(StringBuffer)
            StringBuffer data = new StringBuffer();
            //insertar los datos obtenidos con in en el StringBuffer
            String line = null;
            while ((line = in.readLine()) != null) {
                data.append(line);
            }
/*alternativas para trabajar con json
* https://geekytheory.com/trabajando-con-json-en-android-gson
* crea desde el json al mosdelo en java
* http://www.jsonschema2pojo.org/*/
            JSONObject jsonObject = new JSONObject(data.toString());
            JSONArray genres = jsonObject.getJSONArray("genres");
            for (int i = 0; i < genres.length(); i++) {
                JSONObject jsonMovie = genres.getJSONObject(i);
                String title = null;
                if (jsonMovie.isNull("name") == false) {
                    title = jsonMovie.getString("name");
                } else {
                    title = jsonMovie.getString("key");
                }
                listCategory.add(title);
            }
            return listCategory;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null)
                    in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}

