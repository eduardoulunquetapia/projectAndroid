package com.duard.projectandroid;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Slide;
import android.transition.Transition;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;

import com.duard.projectandroid.adapters.AdapterMovies;
import com.duard.projectandroid.models.Movie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ListMoviesActivity extends AppCompatActivity {
    //https://api.themoviedb.org/3/search/movie?api_key=ffa1e976c5bd23d97f346f2086d22b11&language=es-ES&query=20fiction&include_adult=true&year=2011
    public static final String _URL = "https://api.themoviedb.org/3/search/movie?api_key=";
    public static final String URL_EXTEND = "&include_adult=true";
    public static final String API_KEY = "ffa1e976c5bd23d97f346f2086d22b11";
    public static final String VALUE_CATEGORY = "&query=";
    public static final String VALUE_YEAR = "&year=";
    public static final String VALUE_LANGUAGE = "&language=";

    private RecyclerView recyclerView;
    private String category;
    private String year;
    private String lan;
    private AsyncTaskMovie asyncTaskMovie;
    private FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_list_movies);

        Transition enterTrans = new Slide();
        enterTrans.setDuration(800);
        getWindow().setEnterTransition(enterTrans);

        Transition returnTrans = new Slide();
        returnTrans.setDuration(500);
        getWindow().setExitTransition(returnTrans);
        frameLayout = (FrameLayout) findViewById(R.id.idFrameLayout);
        category = getIntent().getStringExtra("category").toString();
        year = getIntent().getStringExtra("year").toString();
        lan = getIntent().getStringExtra("lan").toString();
        recyclerView = (RecyclerView) findViewById(R.id.idRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        asyncTaskMovie = new AsyncTaskMovie();
        asyncTaskMovie.execute(_URL + API_KEY + URL_EXTEND + VALUE_CATEGORY + category + VALUE_YEAR + year+VALUE_LANGUAGE+lan);
    }
    public void backBeforeActivity(View v){
        Intent intent =new Intent(this,MainActivity.class);
        startActivity(intent);

    }

    public class AsyncTaskMovie extends AsyncTask<String, Void, List<Movie>> {

        @Override
        protected void onPostExecute(List<Movie> listMovies) {
            if (listMovies.isEmpty() || listMovies.size() == 0) {
                recyclerView.setVisibility(View.GONE);
                frameLayout.setVisibility(View.VISIBLE);
            }
            recyclerView.setAdapter(new AdapterMovies(getApplicationContext(), listMovies));
        }

        @Override
        protected List<Movie> doInBackground(String... params) {
            BufferedReader bufferedReader = null;
            List<Movie> movies = null;
            try {
                movies = new ArrayList<Movie>();
                URL url = new URL(params[0]);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                //   Reader reader= new InputStreamReader(con.getInputStream());
                StringBuffer stringBuffer = new StringBuffer();
                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuffer.append(line);
                }
                JSONObject jsonObject = new JSONObject(stringBuffer.toString());
                JSONArray results = jsonObject.getJSONArray("results");
                for (int i = 0; i < results.length(); i++) {
                    JSONObject jsonMovie = results.getJSONObject(i);
                    movies.add(new Movie(
                            Integer.parseInt(jsonMovie.getString("id"))
                            , jsonMovie.getString("original_language")
                            , jsonMovie.getString("original_title")
                            , jsonMovie.getString("poster_path")
                            , jsonMovie.getString("release_date")
                            , Double.parseDouble(jsonMovie.getString("vote_average"))
                            , jsonMovie.getString("overview")));
                }
                return movies;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (bufferedReader != null)
                        bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return movies;
        }
    }

}
