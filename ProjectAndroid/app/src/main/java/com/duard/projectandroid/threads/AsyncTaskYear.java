package com.duard.projectandroid.threads;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.duard.projectandroid.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

/**
 * Created by duard_mac on 14/2/17.
 */

public class AsyncTaskYear extends AsyncTask<String, Void, List<String>> {
    //&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1";
    public static String language;
    //total paginas
    public static final String EXT_PAGE = "&page=";
    public static final int NUMBER_PAGES = 5;//14953;
    BufferedReader bufferdReader = null;
    List<String> listReleaseDate = null;


    private ArrayAdapter<String> spinnerArrayAdapter;

    public AsyncTaskYear(ArrayAdapter<String> spinnerArrayAdapter) {
        this.spinnerArrayAdapter = spinnerArrayAdapter;
    }

    @Override
    protected void onPostExecute(List<String> strings) {
        Collections.sort(strings);
        // se crea y añade lista una lista de años getListYear()
        //  spinnerArrayAdapter.addAll(getListYear());
        // spinnerArrayAdapter.notifyDataSetChanged();
    }

    @Override
    protected List<String> doInBackground(String... params) {
        listReleaseDate = new ArrayList<>();
        try {
            for (int nPages = 1; nPages <= NUMBER_PAGES; nPages++) {
                URL url = new URL(getPageNext(params[0], nPages));
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                bufferdReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                StringBuffer dataJson = new StringBuffer();
                String line = null;
                String release_date = null;
                while ((line = bufferdReader.readLine()) != null) {
                    dataJson.append(line);
                }
                JSONObject jsonObjet = new JSONObject(dataJson.toString());
                JSONArray jsonArray = jsonObjet.getJSONArray("results");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonItems = jsonArray.getJSONObject(i);
                    release_date = getReleaseDateYears(jsonItems.getString("release_date")).toString();
                    listReleaseDate.add(release_date);
                }
            }
            return getListReleaseDate(listReleaseDate);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferdReader != null)
                    bufferdReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private List<String> getListReleaseDate(List<String> listReleaseDate) {
        HashSet<String> hashSet = new HashSet<String>(listReleaseDate);
        listReleaseDate.clear();
        listReleaseDate.addAll(hashSet);
        return listReleaseDate;
    }

    private String getReleaseDateYears(String s) {
        return s.substring(0, 4);
    }

    private String getPageNext(String url, int nPage) {

        return url + EXT_PAGE + nPage;

    }
}
