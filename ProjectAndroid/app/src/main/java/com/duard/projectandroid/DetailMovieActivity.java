package com.duard.projectandroid;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.transition.Slide;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.duard.projectandroid.models.Movie;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class DetailMovieActivity extends AppCompatActivity {
    private Movie movie;
    public static final String URL = "https://api.themoviedb.org/3/movie/";
    public static final String URL_EXTEND = "/credits";
    public static final String API_KEY = "?api_key=ffa1e976c5bd23d97f346f2086d22b11";
    private TextView title;
    private TextView description;
    private ImageView poster_path;
    private TextView credits;
    private TextView vote_average;
    private AsyncTaskCredits asyncTaskCredits;
    private List<String> creditsMovie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_credito_movie);
        Slide exitTrans = new Slide();
        exitTrans.setDuration(1000);
        getWindow().setExitTransition(exitTrans);
        Slide enterTrans = new Slide();
        enterTrans.setDuration(800);
        getWindow().setReenterTransition(enterTrans);
        getWindow().setExitTransition(enterTrans);
        Intent intent = getIntent();
        title = (TextView) findViewById(R.id.idTextViewDetailTitle);
        description = (TextView) findViewById(R.id.idTextViewDetailOverview);
        credits = (TextView) findViewById(R.id.idTextViewDetailCredits);
        vote_average = (TextView) findViewById(R.id.idTextViewDetailAverage);
        poster_path = (ImageView) findViewById(R.id.idImageViewDetailImage);
        asyncTaskCredits = new AsyncTaskCredits();
        creditsMovie = new ArrayList<>();
        asyncTaskCredits.execute(URL + intent.getStringExtra("id") + URL_EXTEND + API_KEY);
        title.setText(intent.getStringExtra("title"));
        String over=intent.getStringExtra("over");
      //  intent.getStringExtra("over").isEmpty()?description.setText("No hay descripcion"):description.setText(intent.getStringExtra("over"));
        if (over==" "||over.isEmpty()) {
            description.setText("No hay descripcion");
        } else {
            description.setText(over);
        }
        vote_average.setText((intent.getStringExtra("vote")).toString());
        Picasso.with(this)
                .load(intent.getStringExtra("poster"))
                .placeholder(R.drawable.camara)
                .centerCrop()
                .resize(500, 550)
                .into(poster_path);
    }

    public class AsyncTaskCredits extends AsyncTask<String, Void, List<String>> {
        @Override
        protected void onPostExecute(List<String> strings) {
            if (strings.isEmpty()) {
                credits.setText("No contiene creditos");
            } else {
                for (int i = 0; i < strings.size(); i++) {
                    credits.append(strings.get(i));
                }
            }
        }

        @Override
        protected List<String> doInBackground(String... params) {

            try {
                URL url = new URL(params[0]);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                StringBuffer data = new StringBuffer();
                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    data.append(line);
                }
                //error  jsonObject no recoge data
                JSONObject jsonObject = new JSONObject(data.toString());
                JSONArray jsonArrayCast = jsonObject.getJSONArray("cast");
                for (int i = 0; i < jsonArrayCast.length(); i++) {
                    JSONObject cast = jsonArrayCast.getJSONObject(i);
                    if (i == 0) {
                        creditsMovie.add("\t\t\t\t\t\t\t\t" + "PERSONAJES\n");
                    }
                    creditsMovie.add(String.format("%1$-35s", cast.getString("character")) + String.format("%1$-30s", "-" + cast.getString("name")) + "\n");
                }
                JSONArray jsonArrayCrew = jsonObject.getJSONArray("crew");
                for (int i = 0; i < jsonArrayCast.length(); i++) {
                    JSONObject crew = jsonArrayCrew.getJSONObject(i);
                    if (i == 0) {
                        creditsMovie.add("\t\t\t\t\t\t\t\t" + "PRODUCCION\n");
                    }
                    creditsMovie.add((crew.getString("department") + "\n") + "\t\t" + (String.format("%1$-30s", crew.getString("job") + ":")) + crew.getString("name") + "\n");
                }
                return creditsMovie;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return creditsMovie;
        }
    }

    private String getOverview(String over) {
        return over.isEmpty() != false ? over : "No hay descripcion";
    }
}
