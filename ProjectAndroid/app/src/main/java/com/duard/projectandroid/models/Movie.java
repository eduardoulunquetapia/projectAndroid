package com.duard.projectandroid.models;

import android.os.Parcelable;

import java.util.List;

public class Movie {
    String poster_path;
    String overview;
    String release_date;
    Integer id;
    String original_title;
    String original_language;
    // String title;
    //  String backdrop_path;
    //  Double popularity;
    // Integer vote_count;
    //  Boolean video;
    //  List<Integer> genre_ids = null;
    // Boolean adult;
    Double vote_average;

    public Double getVote_average() {
        return vote_average;
    }

    public void setVote_average(Double vote_average) {
        this.vote_average = vote_average;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getOriginal_title() {
        return original_title;
    }

    public void setOriginal_title(String original_title) {
        this.original_title = original_title;
    }

    public String getOriginal_language() {
        return original_language;
    }

    public void setOriginal_language(String original_language) {
        this.original_language = original_language;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getOverview() {
        return overview;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Movie(Integer id, String original_language, String original_title, String poster_path, String release_date, Double vote_average, String overview) {

        this.id = id;
        this.original_language = original_language;
        this.original_title = original_title;
        this.poster_path = poster_path;
        this.release_date = release_date;
        this.vote_average = vote_average;
        this.overview = overview;

    }


}