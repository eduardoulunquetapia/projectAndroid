package com.duard.projectandroid.models;

/**
 * Created by duard_mac on 14/2/17.
 */

public class Crew {
    //categoria de oficio o categoria de trabajo
    private static String job_name;
    //nombre del trabajador asignado al oficio
    private static String name;

    public static String getJob_name() {
        return job_name;
    }

    public static void setJob_name(String job_name) {
        Crew.job_name = job_name;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        Crew.name = name;
    }


}
