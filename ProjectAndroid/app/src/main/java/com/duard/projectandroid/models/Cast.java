package com.duard.projectandroid.models;

/**
 * Created by duard_mac on 13/2/17.
 */

public class Cast {
    //personaje
    private static String character;
    //nombre personaje
    private static String name;

    public static String getCharacter() {
        return character;
    }

    public static void setCharacter(String character) {
        Cast.character = character;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        Cast.name = name;
    }


}
