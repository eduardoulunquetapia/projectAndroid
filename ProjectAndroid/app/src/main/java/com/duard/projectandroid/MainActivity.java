package com.duard.projectandroid;

import android.app.ActivityOptions;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Slide;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.duard.projectandroid.threads.AsyncTaskCategory;
import com.duard.projectandroid.threads.AsyncTaskYear;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String URL_YEARS = "https://api.themoviedb.org/3/discover/movie?api_key=";
    public static final String URL_EXTEND = "&sort_by=popularity.desc&include_adult=false&include_video=false";
    public static final String URL_GEN = "https://api.themoviedb.org/3/genre/movie/list";
    public static final String API_KEY = "ffa1e976c5bd23d97f346f2086d22b11";
    public static final String LANGUAGE = "&language=en-US";
    public static final int YEAR_END = 2017;
    public static final int YEAR_START = 1940;
    private Spinner spinner3;
    private Spinner spinner2;
    private Spinner spinner1;
    AsyncTaskYear asyncTaskYear;
    AsyncTaskCategory asyncTaskCategory;
    ArrayAdapter<String> spinnerArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        spinner1 = (Spinner) findViewById(R.id.idSpinner1);
        spinner2 = (Spinner) findViewById(R.id.idSpinner2);
        spinner3 = (Spinner) findViewById(R.id.idSpinner3);
        spinner1.setAdapter(getSpinnerArrayAdapter());
        asyncTaskCategory = new AsyncTaskCategory(spinnerArrayAdapter);
        asyncTaskCategory.execute(URL_GEN + "?api_key=" + API_KEY + LANGUAGE);

        spinner2.setAdapter(getSpinnerArrayAdapterYear());
        /*asyncTaskYear = new AsyncTaskYear(spinnerArrayAdapter);
        asyncTaskYear.execute(URL_YEARS + API_KEY + URL_EXTEND);*/
        spinner3.setAdapter(getSpinnerArrayAdapterLan());
    }

    private SpinnerAdapter getSpinnerArrayAdapter() {
        spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return spinnerArrayAdapter;
    }

    private SpinnerAdapter getSpinnerArrayAdapterYear() {
        spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getListYear()); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return spinnerArrayAdapter;
    }

    private SpinnerAdapter getSpinnerArrayAdapterLan() {
        spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getListLan()); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return spinnerArrayAdapter;
    }

    public void searchListMovies(View view) {
        Slide exitTrans = new Slide();
        exitTrans.setDuration(500);
        getWindow().setExitTransition(exitTrans);
        Slide enterTrans = new Slide();
        enterTrans.setDuration(800);
        getWindow().setReenterTransition(enterTrans);
        getWindow().setExitTransition(enterTrans);
        Intent intent = new Intent(MainActivity.this, ListMoviesActivity.class);
        intent.putExtra("category", spinner1.getSelectedItem().toString());
        intent.putExtra("year", spinner2.getSelectedItem().toString());
        intent.putExtra("lan", spinner3.getSelectedItem().toString());
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
    }

    private List<String> getListLan() {
        List<String> listLan = new ArrayList<String>();
        listLan.add("es-ES");
        listLan.add("de-DE");
        listLan.add("en-US");
        listLan.add("pt-PT");
        listLan.add("fr-FR");
        listLan.add("it-IT");
        return listLan;
    }

    private List<String> getListYear() {
        List<String> listYear = new ArrayList<>();
        for (int i = YEAR_END; i >= YEAR_START; i--) {
            listYear.add(String.valueOf(i));
        }
        return listYear;
    }
}

