package com.duard.projectandroid.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.transition.Slide;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.duard.projectandroid.DetailMovieActivity;
import com.duard.projectandroid.R;
import com.duard.projectandroid.models.Movie;
import com.google.gson.JsonElement;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

/**
 * Created by duard_mac on 15/2/17.
 */

public class AdapterMovies extends RecyclerView.Adapter<AdapterMovies.ViewHolder> {
    public static final String URL_IMAGE = "https://image.tmdb.org/t/p/w500/";
    private Context context;
    private List<Movie> movies;

    public AdapterMovies(Context ctx, List<Movie> movies) {
        this.context = ctx;
        this.movies = movies;
    }

    @Override
    public AdapterMovies.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //se implementa la referencia al row para lista de movies
        LayoutInflater inflater = LayoutInflater.from(context);
        View row = inflater.inflate(R.layout.movies_list_row, parent, false);

        return new ViewHolder(row);
    }

    @Override
    public void onBindViewHolder(AdapterMovies.ViewHolder holder, int position) {
        Picasso.with(context)
                .load(URL_IMAGE + movies.get(position).getPoster_path())
                .resize(500, 500)
                .placeholder(R.mipmap.ic_image1)
                .centerCrop()
                .into(holder.imageView);
        holder.textView.setText(movies.get(position).getOriginal_title());
    }

    @Override
    public int getItemCount() {
        return movies.size() != 0 ? movies.size() : null;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView textView;
        private ImageView imageViewTrailer;

        public ViewHolder(View row) {
            super(row);
            imageView = (ImageView) row.findViewById(R.id.idImageView);
            textView = (TextView) row.findViewById(R.id.idTextView);

            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, DetailMovieActivity.class);
                    intent.putExtra("id", String.valueOf(movies.get(getAdapterPosition()).getId()));
                    intent.putExtra("lan", movies.get(getAdapterPosition()).getOriginal_language());
                    intent.putExtra("title", movies.get(getAdapterPosition()).getOriginal_title());
                    intent.putExtra("poster", URL_IMAGE + movies.get(getAdapterPosition()).getPoster_path());
                    intent.putExtra("date", movies.get(getAdapterPosition()).getRelease_date());
                    intent.putExtra("vote", String.valueOf(movies.get(getAdapterPosition()).getVote_average()));
                    intent.putExtra("over", movies.get(getAdapterPosition()).getOverview());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });










            imageViewTrailer = (ImageView) row.findViewById(R.id.idIcon);
            imageViewTrailer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Intent intent = new Intent(Intent.ACTION_VIEW);
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                URL url = new URL("https://api.themoviedb.org/3/movie/" + String.valueOf(movies.get(getAdapterPosition()).getId()) + "/videos?api_key=ffa1e976c5bd23d97f346f2086d22b11&language=en-US");
                                URLConnection con = url.openConnection();
                                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                                StringBuffer stringBuffer = new StringBuffer();
                                String line = null;
                                while ((line = bufferedReader.readLine()) != null) {
                                    stringBuffer.append(line);
                                }
                                JSONObject jsonObject = new JSONObject(stringBuffer.toString());
                                JSONArray jsonArray = jsonObject.getJSONArray("results");
                                String keyVideo = null;
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject key = jsonArray.getJSONObject(i);
                                    if (i == 0) {
                                        keyVideo = key.getString("key");
                                    }
                                }

                                intent.setData(Uri.parse("https://www.youtube.com/watch?v=" + keyVideo));

                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    thread.start();

                    //thread.join();

                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }
    }

}
