package com.duard.contactprovidertest;

import android.app.ListActivity;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<String> contactId = new ArrayList<>();
        //obtener una lista de los nombres de los contactos
        Cursor cursor = getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI
                , new String[]{
                        ContactsContract.Contacts.DISPLAY_NAME
                        , ContactsContract.CommonDataKinds.Phone.NUMBER}
                , null, null, null);

        List<String> contactName = new ArrayList<>();
        while (cursor.moveToNext()) {
            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            name = name + "-" + cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            contactName.add(name);
        }
        cursor.close();

        Cursor cursorId = getContentResolver().query(
                ContactsContract.Contacts.CONTENT_URI
                , null
                , null
                , null
                , null
        );
        while (cursorId.moveToNext()) {
            String listIs = cursorId.getString(cursorId.getColumnIndex(ContactsContract.Contacts._ID));
            Cursor cMail = getContentResolver().query(
                    ContactsContract.CommonDataKinds.Email.CONTENT_URI//Uri se asigna a la tabla del proveedor
                    , null//projection es una matriz de columnas que debe incluirse para cada fila recuperada.(The columns to return for each row)
                    , ContactsContract.CommonDataKinds.Email.CONTACT_ID + "=?"//selection especifica los criterios para seleccionar filas.
                    , new String[]{listIs}//	(los argumentos de selección reemplazan a los marcadores de posición ? en la cláusula de selección).
                    , null
            );
            while (cMail.moveToNext()) {
                String name = cMail.getString(cMail.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                contactName.add(name);
            }
            cMail.close();

        }


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, contactName);

        setListAdapter(adapter);
    }
}
