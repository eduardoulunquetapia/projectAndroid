package com.duard.asynctaskprueba;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    Button btnStart, btnCancel;
    TextView boxTextView;
    AsyncTaskIterac tareaContador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnStart = (Button) findViewById(R.id.btnStart);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        boxTextView = (TextView) findViewById(R.id.boxTextView);
        tareaContador = new AsyncTaskIterac();
        tareaContador.setContadorTextView(boxTextView);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lanzar();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelar();
            }
        });
    }

    public void lanzar() {
        tareaContador.execute(100);
    }

    public void cancelar() {
        tareaContador.cancel(true);
    }


    // AsyncTask <entrada, progreso, salida>
    class AsyncTaskIterac extends AsyncTask<Integer, Integer, Void> {
        TextView contadorTextView;

        public TextView getContadorTextView() {
            return contadorTextView;
        }

        public void setContadorTextView(TextView contadorTextView) {
            this.contadorTextView = contadorTextView;
        }

        public AsyncTaskIterac() {
            super();
        }

        @Override
        protected Void doInBackground(Integer... params) {
            for (int i = 0; i < params[0]; i++) {
                if (isCancelled()) {
                    break;
                } else {
                    try {
                        Thread.sleep(2000);
                        //publico el progreso
                        publishProgress(i);
                    } catch (InterruptedException e) {
                        Log.d("A", "a");
                    }
                }
            }
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();

        }

        @Override
        protected void onCancelled(Void aVoid) {
            super.onCancelled(aVoid);
            // se ejecuta cuando presiona el boton cancelar
            //llama a este metodo OnCancelled(Void aVoid ) con parametro
            contadorTextView.setText("Cancelado");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //cuando termina la alicacion
            contadorTextView.setText("Cancelado");
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            contadorTextView.setText(values[0].toString());
        }
    }
}

