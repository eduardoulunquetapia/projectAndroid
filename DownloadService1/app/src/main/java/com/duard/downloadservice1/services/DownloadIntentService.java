package com.duard.downloadservice1.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.os.Environment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

import com.duard.downloadservice1.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import static android.provider.ContactsContract.CommonDataKinds.Website.URL;


public class DownloadIntentService extends IntentService {

    public static final String TAG = "DownloadIntentService";

    public static final String URL_EXTRA = "url";
    private static final String EXTRA_PARAM1 = "";
    private static final String EXTRA_PARAM2 = "";
    private static final String ACTION_FOO = "";
    private static final String ACTION_BAZ = "";

    public DownloadIntentService() {
        super("DownloadIntentService");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionFoo(Context context, String param1, String param2) {
        Intent intent = new Intent(context, DownloadIntentService.class);
        intent.setAction(ACTION_FOO);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionBaz(Context context, String param1, String param2) {
        Intent intent = new Intent(context, DownloadIntentService.class);
        intent.setAction(ACTION_BAZ);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    public static Intent makeIntent(URL url) {
        return null;
    }

    public static Intent makeIntent(String url) {
        return null;
    }

    public static Intent makeIntent(Context ctx, URL url) {
        Intent intent = new Intent(ctx, DownloadIntentService.class);
        intent.putExtra(URL_EXTRA, url);
        return intent;
    }

    public static Intent makeIntent(Context ctx, String url) {
        Intent intent = new Intent(ctx, DownloadIntentService.class);
        intent.putExtra(URL_EXTRA, url);
        return intent;
    }


    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String urlStr = intent.getStringExtra(URL_EXTRA);
        OutputStream out = null;
        File file = null;
        try {
            URL url = new URL(urlStr);
            URLConnection con = url.openConnection();
            InputStream in = con.getInputStream();
            out=openFileOutput("download",MODE_PRIVATE);
            file = openOutputStream(url);
            if (file == null) {
                Log.d(TAG, "No se pudo acceder al espacio de almacenamiento externo...");
                file = Environment.getDataDirectory();
                out = openFileOutput(url.toString().replace("/", "_"), MODE_PRIVATE);
            } else {
                out = new FileOutputStream(file);
            }

            byte[] buffer = new byte[1024];
            int bytesLeidos;
            do {
                bytesLeidos = in.read(buffer);
                out.write(buffer, 0, bytesLeidos);
            } while (bytesLeidos == buffer.length);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            Notification.Builder builder = new Notification.Builder(this);
            file=Environment.getDataDirectory();
            builder
                    .setSmallIcon(R.drawable.ic_download)
                    .setContentTitle("download finished")
                    .setContentText("descarga terminada en "+file.getAbsolutePath());

            Log.d(TAG, " ruta  "+file.getAbsolutePath());
            Notification notification = builder.build();
            NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            nm.notify(11111, notification);
            try {
                if (out != null) out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    private File openOutputStream(URL url) {
        Uri uri = Uri.parse(url.toString());
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File imageAndMovieDir = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DCIM + "/MisImagenes");
            if (!imageAndMovieDir.exists())
                imageAndMovieDir.mkdir();
            return new File(imageAndMovieDir, uri.getLastPathSegment());
        }
        return null;
    }
}