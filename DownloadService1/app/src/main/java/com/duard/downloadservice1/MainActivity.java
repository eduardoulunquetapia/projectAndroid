package com.duard.downloadservice1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.duard.downloadservice1.services.DownloadIntentService;

public class MainActivity extends AppCompatActivity {
    private EditText download;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        download = (EditText) findViewById(R.id.urlEditText);
    }

    public void startDownload(View v) {
        /*permiso para acceso a internet en MANIFEST.xmml
        <uses-permission android:name="android.permission.INTERNET"></uses-permission>*/
        String url = download.getText().toString();
        Intent intent = DownloadIntentService.makeIntent(this, url);
        startService(intent);
    }
}
