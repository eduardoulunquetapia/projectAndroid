package com.duard.admobbanner;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

public class MainActivity extends AppCompatActivity {
    private AdView adview1, adview2;
    private Button btnBanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnBanner = (Button) findViewById(R.id.btnId);
        //MobileAds.initialize(this);
        adview1 = new AdView(this);
        adview1 = (AdView) findViewById(R.id.idAdViewId1);
        btnBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                adview1.loadAd(new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build());
            }
        });

    }

    public void publishBanner2(View v) {
        adview2 = new AdView(this);
        adview2 = (AdView) findViewById(R.id.adViewId2);
        /*adview2.setAdSize(AdSize.BANNER);
        adview2.setAdUnitId("ca-app-pub-2441386146736393/7225180266");*/
        AdRequest adRequest = new AdRequest
                .Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        adview2.loadAd(adRequest);
    }
}
