package com.duard.carrecycleview.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.duard.carrecycleview.CarDetailActivity;
import com.duard.carrecycleview.R;
import com.duard.carrecycleview.model.Car;

import java.util.List;

/**
 * Created by cice on 20/1/17.
 */

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.ViewHolder> {

    private List<Car> carList;
    private Context ctx;

    public CarAdapter(Context ctx, List<Car> list){
        carList=list;
        this.ctx=ctx;
    }
    /*
    * este metodo se llama cada vez que sea necesario construir una nueva fila*/

    @Override
    public CarAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater=LayoutInflater.from(ctx);
        View row=inflater.inflate(R.layout.car_row, parent,false);
        //ViewHolder holder=new ViewHolder(row);
        return new ViewHolder(row);
    }

    @Override
    public void onBindViewHolder(CarAdapter.ViewHolder holder, int position) {
        holder.carImageView.setImageResource(carList.get(position).getThumb());
        holder.carTextView.setText(carList.get(position).getFabricante()+ " "+
        carList.get(position).getModelo());
    }

    @Override
    public int getItemCount() {
        return carList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView carImageView;
        private TextView carTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            carImageView=(ImageView) itemView.findViewById(R.id.carThumbIdView);
            carTextView=(TextView) itemView.findViewById(R.id.carNameTextView);
            carImageView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    Log.d("CarViewHolder", "old position: "+getOldPosition());
                    Log.d("CarViewHolder", "layout position: "+getLayoutPosition());
                    Log.d("CarViewHolder", "adapter position: "+getAdapterPosition());
                    Intent intent =new Intent(ctx, CarDetailActivity.class);
                    intent.putExtra(CarDetailActivity.IMAGE_EXTRA,carList.get(getAdapterPosition()).getImage());
                    intent.putExtra(CarDetailActivity.DESCRIPCION_EXTRA,carList.get(getAdapterPosition()).getDescripcion());
                    intent.putExtra(CarDetailActivity.FABRICANTE_EXTRA,carList.get(getAdapterPosition()).getFabricante());
                    intent.putExtra(CarDetailActivity.MODELO_EXTRA,carList.get(getAdapterPosition()).getModelo());

                    ctx.startActivity(intent);
                }
            });

        }
    }
}
