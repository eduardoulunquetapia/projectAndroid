package com.duard.carrecycleview;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class CarDetailActivity extends AppCompatActivity {
    public static final String IMAGE_EXTRA="image";
    public static final String MODELO_EXTRA="modelo";
    public static final String FABRICANTE_EXTRA="fabricante";
    public static final String DESCRIPCION_EXTRA="descripcion";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_detail);

        Intent intent=getIntent();
        int imageResource =intent.getIntExtra(IMAGE_EXTRA,0);
        String modelo =intent.getStringExtra(MODELO_EXTRA);
        String fabricante =intent.getStringExtra(FABRICANTE_EXTRA);
        String descripcion=intent.getStringExtra(DESCRIPCION_EXTRA);

        ImageView carImage=(ImageView) findViewById(R.id.carImage);
        TextView carDescripcion=(TextView)findViewById(R.id.carDetail);
        if(imageResource!=0){
            carImage.setImageResource(imageResource);
            carDescripcion.setText("Fabricante: "+fabricante+"\n"+"modelo "+modelo);

        }

    }
}

