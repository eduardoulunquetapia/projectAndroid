package com.duard.carrecycleview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.duard.carrecycleview.adapter.CarAdapter;
import com.duard.carrecycleview.model.Car;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    List<Car> data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CarAdapter adapter=new CarAdapter(this, data);
        RecyclerView carRV= (RecyclerView) findViewById(R.id.carRecycleView);

        carRV.setAdapter(new CarAdapter(this, getData()));
        carRV.setLayoutManager(new LinearLayoutManager(this));
    }
    private List<Car> getData(){
        List<Car> list =new ArrayList<>();
        list.add(new Car("bla bla bla", "peugot", R.drawable.car1,"307",R.drawable.car1_thumbnail));
        list.add(new Car("bla bla bla", "nissa", R.drawable.car2,"307",R.drawable.car2_thumbnail));
        list.add(new Car("bla bla bla", "honda", R.drawable.car3,"307",R.drawable.car3_thumbnail));
        list.add(new Car("bla bla bla", "volvo", R.drawable.car4,"307",R.drawable.car4_thumbnail));
        list.add(new Car("bla bla bla", "opel", R.drawable.car5,"307",R.drawable.car5_thumbnail));
        list.add(new Car("bla bla bla", "mazda", R.drawable.car6,"307",R.drawable.car6_thumbnail));
        list.add(new Car("bla bla bla", "toyota", R.drawable.car7,"307",R.drawable.car7_thumbnail));



        return list;
    }
}
