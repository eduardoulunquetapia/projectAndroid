package com.duard.friendsdynamiclistview;

import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ArrayAdapter<String> adapter;
    private ListView friendsLV;
    private EditText friendET;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<String> listIni = new ArrayList<String>();
        listIni.add("juan");
        listIni.add("edu");
        listIni.add("ana");
        listIni.add("eva");
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                android.R.id.text1,
                listIni);

        friendsLV = (ListView) findViewById(R.id.friendsLV);
        friendsLV.setAdapter(adapter);
        friendET = (EditText) findViewById(R.id.newNameET);

    }
        public void addFriend(View v){
            String newFriend= friendET.getText().toString();
        adapter.add(newFriend);
        adapter.notifyDataSetChanged();
        friendET.setText("");

         }

    }

