package com.duard.toolbar.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.duard.toolbar.R;

/**
 * Created by cice on 26/1/17.
 */
//hay que heredar de DiaalogFragment para crear dialogos
public class AddCarDialog extends DialogFragment {
    private AddCarInterface mContext;
    private EditText modeloET, fabricanteET;

    public interface AddCarInterface {
        public void addCar(String modelo, String fabricante);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mContext = (AddCarInterface) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("el contexto no implementa el interfaz AddCarInterface");
        }

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builer = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layout = inflater.inflate(R.layout.add_car_dialog_layout, null);
        modeloET = (EditText) layout.findViewById(R.id.modeEditText);
        fabricanteET = (EditText) layout.findViewById(R.id.fabricanteEditText);
        builer
                .setMessage(R.string.add_car_dialog_title)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String modelo=modeloET.getText().toString();
                        String fabricante=fabricanteET.getText().toString();
                        mContext.addCar(modelo,fabricante);

                    }
                })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).setView(layout);
        return builer.create();
    }
}
