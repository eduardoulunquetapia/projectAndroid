package com.duard.toolbar.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.duard.toolbar.R;
import com.duard.toolbar.model.Car;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by cice on 20/1/17.
 * ----recycleView no tiene filtro asociado
 * ----
 */

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.ViewHolder> implements Filterable {

    public List<Car> carList;
    private Context ctx;

    public CarAdapter(Context ctx, List<Car> list) {
        carList = list;
        this.ctx = ctx;
    }
    /*
    * este metodo se llama cada vez que sea necesario construir una nueva fila*/

    @Override
    public CarAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //obtenemos el infater necesario para construir un fila definida en xml
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View row = inflater.inflate(R.layout.car_row, parent, false);
        ViewHolder holder = new ViewHolder(row);

        return holder;
    }

   /* public void appendCar(String model, String fabricante) {
        Car car = new Car(null, fabricante, R.drawable.ic_car, R.drawable.ic_car, model);
        carList.add(car);
        notifyDataSetChanged();
    }*/

    public void addCar(String modelo, String fabricante) {

        Car car = new Car(null, fabricante, R.drawable.ic_car, modelo, R.drawable.ic_car);
        carList.add(car);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final CarAdapter.ViewHolder holder, int position) {
        holder.carImageView.setImageResource(carList.get(position).getThumb());
        holder.carTextView.setText(carList.get(position).getFabricante() + " " +
                carList.get(position).getModelo());
        holder.vElipsisTextView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //aqui donde sale el pop Up
                Log.d("elipsis", "onClick().....");
                PopupMenu popup = new PopupMenu(ctx, holder.vElipsisTextView);
                popup.inflate(R.menu.car_popup);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.deleteCarItem:
                                //borrar de la lista el coche
                                Log.d("Elipsis", "delete...");
                                carList.remove(holder.getAdapterPosition());
                                notifyDataSetChanged();

                                break;
                            case R.id.detailCarItem:
                                break;
                        }
                        return true;
                    }
                });
                popup.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return carList.size();
    }

    @Override
    public Filter getFilter() {

        return new CarFilter();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView carImageView;
        private TextView carTextView;
        private TextView vElipsisTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            carImageView = (ImageView) itemView.findViewById(R.id.carThumbIdView);
            carTextView = (TextView) itemView.findViewById(R.id.carNameTextView);
            vElipsisTextView = (TextView) itemView.findViewById(R.id.vElipsis);


            carImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("CarViewHolder", "old position: " + getOldPosition());
                    Log.d("CarViewHolder", "layout position: " + getLayoutPosition());
                    Log.d("CarViewHolder", "adapter position: " + getAdapterPosition());
                   /* Intent intent =new Intent(ctx, CarDetailActivity.class);
                    intent.putExtra(CarDetailActivity.IMAGE_EXTRA,carList.get(getAdapterPosition()).getImage());
                    intent.putExtra(CarDetailActivity.DESCRIPCION_EXTRA,carList.get(getAdapterPosition()).getDescripcion());
                    intent.putExtra(CarDetailActivity.FABRICANTE_EXTRA,carList.get(getAdapterPosition()).getFabricante());
                    intent.putExtra(CarDetailActivity.MODELO_EXTRA,carList.get(getAdapterPosition()).getModelo());

                    ctx.startActivity(intent);*/
                }
            });

        }
    }

    public class CarFilter extends Filter {
        public static final String TAG = "CarFilter";
        private List<Car> originalList;
        private List<Car> filteredList;

        public CarFilter() {
            originalList = new LinkedList<>(carList);

        }

        @Override
        protected FilterResults performFiltering(CharSequence filterData) {
            Log.d(TAG, "performFiltering()...");
            String filterStr = filterData.toString();
            FilterResults results = new FilterResults();
            filteredList = new ArrayList<>();
            for (Car car : originalList) {
                if (car.getFabricante().equalsIgnoreCase(filterStr) ||
                        car.getModelo().equalsIgnoreCase(filterStr)) {
                    filteredList.add(car);
                }
            }
            results.values = filteredList;
            results.count = filteredList.size();
            //Log.d(TAG, "performFiltering()...");
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            Log.d(TAG, "publishResult()...");
            List<Car> list = (List<Car>) results.values;

            if (list.size() > 0) {
                carList = list;
            }
            notifyDataSetChanged();
        }
    }
}
