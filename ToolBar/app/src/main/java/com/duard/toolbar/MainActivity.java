package com.duard.toolbar;

import android.content.Context;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.duard.toolbar.adapter.CarAdapter;
import com.duard.toolbar.dialogs.AddCarDialog;
import com.duard.toolbar.model.Car;

import java.util.ArrayList;
import java.util.List;

import static android.R.attr.data;

public class MainActivity extends AppCompatActivity implements AddCarDialog.AddCarInterface {
    public static final String TAG = "MainActivity";
    private EditText searchEditText;
    private ActionBar eBar;
    //private ArrayAdapter adapter;
    private CarAdapter adapter;
    private List<Car> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.includedToolbar);
        setSupportActionBar(toolbar);
        eBar = getSupportActionBar();
        RecyclerView carRV = (RecyclerView) findViewById(R.id.carRecycleView);
        data=getData();
        adapter = new CarAdapter(this, data);
        carRV.setAdapter(adapter);
        carRV.setLayoutManager(new LinearLayoutManager(this));
        //  llama a performFiltering y luego  publishResult
        //  this.adapter.getFilter().filter("fwfwf");

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // eBar=getSupportActionBar();
        switch (item.getItemId()) {
            case R.id.searchItem:
                Log.d(TAG, "search item");
                eBar.setDisplayShowCustomEnabled(true);
                //añade la caja de texto
                eBar.setCustomView(R.layout.search_layout);
                //quita el titulo
                eBar.setDisplayShowTitleEnabled(false);
                searchEditText = (EditText) eBar.getCustomView().findViewById(R.id.searchEditText);
                searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                            CharSequence searchText = searchEditText.getText();
                            Log.d(TAG, "search ::..." + searchText);
                            //referencia del sistema del servicio
                            InputMethodManager imn = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imn.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);
                            //quita la caja de texto
                            eBar.setDisplayShowCustomEnabled(false);
                            //añade el titulo
                            eBar.setDisplayShowTitleEnabled(true);
                            //empezar la busqueda
                            adapter.getFilter().filter(searchText);
                            return true;
                        }
                        return false;
                    }
                });
                InputMethodManager imn = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                //muestra caja teclado
                imn.showSoftInput(searchEditText, InputMethodManager.SHOW_IMPLICIT);
                searchEditText.requestFocus();
                break;
            case R.id.settingItem:
                //  eBar.setDisplayShowCustomEnabled(false);
                //  eBar.setDisplayShowTitleEnabled(true);
                Log.d(TAG, "setting item");
                break;
            case R.id.aboutItem:
                //lanzar actividad desde aqui
                //        openAbout();
                Log.d(TAG, "about item...");
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(TAG, "onCreateOption()...");
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    public void openAbout() {

    }

    private List<Car> getData() {
        List<Car> list = new ArrayList<>();
        list.add(new Car("bla bla bla", "peugot", R.drawable.car1, "307", R.drawable.car1_thumbnail));
        list.add(new Car("bla bla bla", "nissa", R.drawable.car2, "307", R.drawable.car2_thumbnail));
        list.add(new Car("bla bla bla", "honda", R.drawable.car3, "307", R.drawable.car3_thumbnail));
        list.add(new Car("bla bla bla", "volvo", R.drawable.car4, "307", R.drawable.car4_thumbnail));
        list.add(new Car("bla bla bla", "opel", R.drawable.car5, "307", R.drawable.car5_thumbnail));
        list.add(new Car("bla bla bla", "mazda", R.drawable.car6, "307", R.drawable.car6_thumbnail));
        list.add(new Car("bla bla bla", "toyota", R.drawable.car7, "307", R.drawable.car7_thumbnail));


        return list;
    }

    @Override
    public void addCar(String modelo, String fabricante) {
        Car car =new Car(null, fabricante, R.drawable.ic_car, modelo,R.drawable.ic_car);
        data.add(car);
        adapter.notifyDataSetChanged();
    }
    // accion en el click de "+"
    public void addCar(View v){
        Log.d(TAG,"addCar()..");
        AddCarDialog dialog=new AddCarDialog();
        dialog.show(getFragmentManager(),"AddCarDialog");
    }
}
